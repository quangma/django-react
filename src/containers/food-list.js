import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {seleteFood} from "../actions";

function mapStateToProps(state) {
    return {
        foods: state.foods
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({seleteFood: seleteFood}, dispatch);
}

class FoodList extends Component {
    createFoodListItems() {
        let listItems = this.props.foods.map(
            (x) => {
                return (
                    <li key={x.id} onClick={() => {
                        this.props.seleteFood(x)
                    }}>
                        Name: {x.name}
                    </li>
                );
            }
        );
        return (listItems);
    }

    render() {
        return (
            <ul>
                {this.createFoodListItems()}
            </ul>
        );
    }
}

let FoodContainer = connect(
    mapStateToProps, mapDispatchToProps
)(FoodList);

export default FoodContainer;