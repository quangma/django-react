const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const config = require('./webpack.base.js');

function resolve(relatedPath) {
    return path.join(__dirname, relatedPath)
}

config.mode = 'development';

config.entry = {
    'main': [
        'webpack-dev-server/client?http://127.0.0.1:3000',
        'webpack/hot/only-dev-server',
        resolve('src/index.js')
    ]
};

config.output = {
    path: resolve('dist'),
    filename: 'js/[name].bundle.js',
    publicPath: 'http://127.0.0.1:3000/dist/',
};

config.devtool = 'eval';

config.plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new BundleTracker({filename: resolve('webpack-stats.json')}),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('development'),
            BASE_URL: JSON.stringify('http://127.0.0.1:8000/'),
        }
    }),
    new MiniCssExtractPlugin({
        filename: 'css/[name].bundle.css'
    })
];

config.devServer = {
    inline: true,
    hot: true,
    historyApiFallback: true,
    host: '127.0.0.1',
    port: 3000,
    headers: {'Access-Control-Allow-Origin': '*'}
};

module.exports = config;