from .settings_base import *

ALLOWED_HOSTS += []

DEBUG = False

STATIC_ROOT = os.path.join(BASE_DIR, 'dist/static_root')
MEDIA_ROOT = os.path.join(BASE_DIR, 'public/media_root')

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]
