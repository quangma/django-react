from .settings_base import *

ALLOWED_HOSTS += ['127.0.0.1']

DEBUG = True

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': '',
        'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats.json'),
    }
}

CORS_ORIGIN_WHITELIST = (
    '127.0.0.1:3000',
)
