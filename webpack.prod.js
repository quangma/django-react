const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const config = require('./webpack.base.js');

config.mode = 'production';

config.plugins = [
    new BundleTracker({filename: './frontend/webpack/webpack-stats.production.json'}),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('production'),
            BASE_URL: JSON.stringify('http://0.0.0.0/'),
        }
    }),
    new webpack.LoaderOptionsPlugin({
        minimize: true
    }),
    new webpack.optimize.UglifyJsPlugin({
        mangle: false,
        sourcemap: true,
        compress: {
            warnings: true
        }
    })
];

module.exports = config;