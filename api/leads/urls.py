from django.urls import path, include
from .views import LeadListView, LeadDetailView

urlpatterns = [
    path('', LeadListView.as_view()),
    path('<int:pk>/', LeadDetailView.as_view()),
]
